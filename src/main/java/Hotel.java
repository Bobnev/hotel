import java.io.*;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.*;

public class Hotel implements Comparable<Hotel>{
    private String name;
    private String stadt;
    private int size;
    private boolean smoking;
    private int rate;
    private LocalDate date;
    private String besitzer;

    public Hotel(String name, String stadt, int i, boolean smoking, int i1, LocalDate date, String besitzer) {
        this.name = name;
        this.stadt = stadt;
        this.size = i;
        this.smoking = smoking;
        this.rate = i1;
        this.date = date;
        this.besitzer = besitzer;
    }

    public Hotel(byte[] data, Map<String, Short> columns) {
        List<String> l=new ArrayList<>();
        int i1=0;
        for (String s:columns.keySet()) {
            byte[] b = new byte[columns.get(s)];
            for (int i=0; i <columns.get(s) ; i++) {
                b[i]=data[i1];
                i1++;
            }
            String x= new String(b, StandardCharsets.UTF_8);
            l.add(x.trim());
        }
        this.name = l.get(0);
        this.stadt = l.get(1);
        this.size = Integer.parseInt(l.get(2));
        this.smoking = l.get(3).equals("Y");
        this.rate = Integer.parseInt(l.get(4).substring(1,l.get(4).length()-3))*100;
        String[] s= l.get(5).split("/");
        this.date = LocalDate.of(Integer.parseInt(s[0]), Integer.parseInt(s[1]), Integer.parseInt(s[2]));
        this.besitzer = l.get(6);

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Hotel)) return false;
        Hotel hotel = (Hotel) o;
        return size == hotel.size && smoking == hotel.smoking && rate == hotel.rate && name.equals(hotel.name) && stadt.equals(hotel.stadt) && date.equals(hotel.date) && Objects.equals(besitzer, hotel.besitzer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, stadt, size, smoking, rate, date, besitzer);
    }

    public static Set<Hotel> readHotels(String filename) throws IOException {
        Set<Hotel> set = new TreeSet<>();
        Map<String, Short> columns = readColumns(filename);
        try (DataInputStream data = new DataInputStream(new FileInputStream(filename))) {
            int id = data.readInt();
            int offset = data.readInt();
            int bytes = 2 + Integer.BYTES;
            data.skipBytes(offset - 8);
            while (data.available() > 0){
                int checkDeleted = data.readUnsignedShort();
                if (checkDeleted == 0x8000) {
                    data.skipBytes(159);
                } else if (checkDeleted == 0x0000){
                    byte[] hotels = new byte[159];
                    data.read(hotels);
                    set.add(new Hotel(hotels, columns));
                } else {
                    throw new IllegalArgumentException(filename);
                }
            }
        } catch (FileNotFoundException e) {
            System.err.println(e + " : " + filename);
        }
        return set;
    }

    public static Map<String, Short> readColumns(String filename) throws IOException {
        Map<String, Short> map = new LinkedHashMap<>();
        try (DataInputStream dataInputStream = new DataInputStream(new FileInputStream(filename))) {
            int id = dataInputStream.readInt();
            int offset = dataInputStream.readInt();
            short columnCount = dataInputStream.readShort();
            byte[] bytes;
            for (int i = 0; i < columnCount; i++) {
                dataInputStream.readByte();
                byte nameSize = dataInputStream.readByte();
                bytes = new byte[nameSize];
                dataInputStream.read(bytes);
                map.put(new String(bytes, StandardCharsets.UTF_8),dataInputStream.readShort());
            }
        } catch (FileNotFoundException e) {
            System.err.println(e + " : " + filename);
        }
        return map;
    }

    public static int getStartingOffset(String db) {
        try (DataInputStream dataInputStream=new DataInputStream(new FileInputStream(db))){
            dataInputStream.readInt();
            return dataInputStream.readInt();

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public int compareTo(Hotel o) {
        if(o.stadt.compareTo(this.stadt)<0) {
            return 1;
        }
        else if(o.stadt.compareTo(this.stadt)>0){
            return -1;
        }
        else {
            return -o.name.compareTo(this.name);
        }
    }

}
